# lw.jl
lw.jl是用于计算带电粒子在任一观测方向`n`产生的辐射谱的[julia](https://julialang.org/) package。

## 安装
``` julia
julia>]
pkg> add gitlab.com/xsgeng/lw.jl
```
## 使用方法
``` julia
using lw
```

lw.jl中`c = 1, m = 1, e = 1`，数据需转换后使用。

- 从hdf5文件中读取原始粒子轨迹
    ```
    particles = h5readparticles("trajectories.h5")
    ```
    文件`trajectories.h5`需包含`nt`行、`nparticle`列的数组`x,y,z,px,py,pz`，以及时间步长标量`dt`，或者`dt`作为attribute存储在`/`下

- 直接创建粒子轨迹
    ```julia
    particles = Particles(x, y, z, px, py, pz, dt, size(x)...)
    ```

- 提取轨迹用于后处理
    ```julia
    traj = extract(particles)
    ```
    或者提取选定的轨迹
    ```julia
    mask = particles.x > 5
    traj = extract(particles, mask)
    ```

- 计算能谱

    为计算能谱，需要定义探测器所在的方向、探测时间间隔和探测的时长

    例如
    ```julia
    nθ = 128

    φ = 0
    θs = LinRange(0, 0.003, nθ)
    detectors = [[cos(θ); sin(θ) * cos(φ); sin(θ) * sin(φ)] for θ = θs]

    γ = 1000
    γ² = γ^2

    # 输出能谱的长度和截止能量
    nω = 256
    ωₘ = 30E6 / 1.55 * 2.35E15

    # dt_detector能够分辨最大频率ωₘ
    dt_detector = π / ωₘ / 2
    dur_detector = 50E-15 / 4γ²

    # 非相干能谱
    spec_out = IncoherentSpectrum(traj, detectors, dt_detector, dur_detector, nω, ωₘ)
    # 相关能谱
    spec_out = CoherentSpectrum(traj, detectors, dt_detector, dur_detector, nω, ωₘ)
    ```

    `IncoherentSpectrum`和`CoherentSpectrum`返回结果为`Vector{Spectrum}`，对应`detectors`。

    其中`Spectrum`包含振幅和频率坐标轴
    
    ```julia
    struct Spectrum
        amp::Vector{Float64}
        ω::Vector{Float64}
    end
    ```
