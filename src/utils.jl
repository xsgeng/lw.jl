export Trajectory, Particles, Spectrum, h5readparticles, extract, dumpspectrum


struct Particles
    x::Array{Float64,2}
    y::Array{Float64,2}
    z::Array{Float64,2}
    px::Array{Float64,2}
    py::Array{Float64,2}
    pz::Array{Float64,2}
    dt::Float64
    sz::Int
    nt::Int
end
function extract(part::Particles, ipart::Int)
    # time steps
    t = [t for t = 0:part.nt - 1] * part.dt
    # selection
    s = [i for i = 2:part.nt - 1]

    r = [part.x[ipart:ipart, :]; part.y[ipart:ipart, :]; part.z[ipart:ipart, :]]
    p = [part.px[ipart:ipart, :]; part.py[ipart:ipart, :]; part.pz[ipart:ipart, :]]

    β = p ./ sqrt.(p ⋅ p ./ (m^2 * c²) .+ 1) / m / c
    a = (β[:, s .+ 1] .- β[:, s .- 1]) ./ (t[3] - t[1])

    return Trajectory(
        t[s],
        r[:, s],
        β[:, s],
        a,
        part.dt,
        part.nt-2,
    )
end
function extract(part::Particles)
    traj::Vector{Trajectory} = []
    for ipart = 1:part.sz
        push!(traj, extract(part, ipart))
    end
    return traj
end
function extract(part::Particles, mask::BitArray{1})
    traj::Vector{Trajectory} = []
    for ipart = (1:part.sz)[mask]
        push!(traj, extract(part, ipart))
    end
    return traj
end

struct Trajectory
    t::Vector{Float64}
    r::Array{Float64,2}
    β::Array{Float64,2}
    a::Array{Float64,2}
    dt::Float64
    nt::Int
end
gamma(traj::Trajectory) = sqrt.(traj.p ⋅ traj.p ./ (m^2 * c²) .+ 1)
vel(traj::Trajectory) = traj.p ./ m ./ gamma(traj)

struct Detector
    E::Array{Float64,2}
    nt::Int
    dt::Float64
    dur::Float64
    ts::Union{Vector{Float64}, AbstractRange{Float64}}
    lock::Threads.SpinLock
    Detector(nt::Int, dt::Float64, dur::Float64, ts::Union{Vector{Float64}, AbstractRange{Float64}}) = new(
        zeros(3, nt),
        nt, dt, dur, ts,
        Threads.SpinLock(),
    )
end
function reset!(detector::Detector)
    detector.E .= 0
end

struct Spectrum
    amp::Vector{Float64}
    ω::Vector{Float64}
end
function add!(s1::Spectrum, s2::Spectrum)
    interp = LinearInterpolation(s2.ω, s2.amp, extrapolation_bc=0)
    s1.amp .+= interp(s1.ω)
end
function dumpspectrum(spectrums::Vector{Spectrum}, filename::AbstractString="data")
    open(filename, "w") do file
        for s = spectrums
            for a = s.amp[1:end-1]
                write(file, @sprintf("%.10E, ", a))
            end
            write(file, @sprintf("%.10E\n", s.amp[end]))
        end
    end
end

"""
`h5readparticles(filename::String)` reads all trajectories of x, y, [z], px, py, [pz] and dt from hdf5 file.

`h5readparticles(filename::String, id::AbstractRange{Int})` reads selected particles from `id`.


- The module process data in units of `c = 1`, `e = 1`, `m = 1`; convert before save to hdf5.
- The trajectories must be stored in arrays of `nt` rows and `npart` cols in the `"/"` of the file.
- `dt` can be a `1x1` scalar or an attribute at `"/"`
"""
function h5readparticles(filename::String)
    file = h5open(filename, "r")
    x = file["x"][:, :]
    y = file["y"][:, :]
    if haskey(file, "z")
        z = file["z"][:, :]
    else
        z = 0 * y
    end

    px = file["px"][:, :]
    py = file["py"][:, :]
    if haskey(file, "pz")
        pz = file["pz"][:, :]
    else
        pz = 0 * py
    end
    
    if haskey(file, "dt")
        dt = file["dt"]
        if isa(dt, AbstractArray)
            dt = dt[1]
        end
        close(file)
    else
        dt = h5readattr(filename, "/")["dt"]
        @assert !isempty(dt) "dt is not stored in $filename"
        close(file)
    end
    

    return Particles(x, y, z, px, py, pz, dt, size(x)...)
end

function h5readparticles(filename::String, id::AbstractRange{Int})
    file = h5open(filename, "r")
    x = file["x"][id, :]
    y = file["y"][id, :]
    if exists(file, "z")
        z = file["z"][id, :]
    else
        z = 0 * y
    end

    px = file["px"][id, :]
    py = file["py"][id, :]
    if exists(file, "pz")
        pz = file["pz"][id, :]
    else
        pz = 0 * py
    end

    if exists(file, "dt")
        dt = file["dt"]
        if isa(dt, AbstractArray)
            dt = dt[1]
        end
        close(file)
    else
        dt = h5readattr(filename, "/")["dt"]
        @assert !isempty(dt) "dt is not stored in $filename"
        close(file)
    end

    return Particles(x, y, z, px, py, pz, dt, size(x)...)
end

function radtime(n::Vector{Float64}, trajectories::Vector{Trajectory})
    # t - n⋅r/c
    rad_start = minimum([traj.dt * 1       .- n⋅traj.r[:, 1]   / c for traj = trajectories])
    rad_end = minimum([traj.dt * traj.nt .- n⋅traj.r[:, end] / c for traj = trajectories])

    rad_dur = rad_end - rad_start

    return rad_start, rad_end, rad_dur

end
