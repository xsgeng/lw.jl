module lw
using Interpolations
using FFTW
using HDF5
using LinearAlgebra
using Printf

include("vec3.jl")
include("utils.jl")
include("constants.jl")
include("algorithms.jl")

export getfields, getspectrum, interpfield!


@inline function getfields(traj::Trajectory, n::Vector{Float64})
    @views t_ret = traj.t' .- n ⋅ traj.r / c
    @views E = n × ((n .- traj.β) × traj.a) ./ (1 .- n ⋅ traj.β).^3
    return E, t_ret[:]
end

function interpfield!(
    detector::Detector,
    E_ret::Array{Float64,2},
    t_ret::Union{Vector{Float64}, AbstractRange{Float64}};
    synchronize::Bool = false,
)
    if all(isnan.(t_ret))
        return
    end
    # filter nan
    data_start = findfirst(@. ~isnan(t_ret))
    data_end   = findlast(@. ~isnan(t_ret))

    tid_start = floor(Int, t_ret[data_start] / detector.dt) + 1
    tid_end = floor(Int, t_ret[data_end] / detector.dt) + 1
    # omit points outside dur_detector
    if tid_end*detector.dt > detector.dur
        tid_end = detector.nt
    end
    t_linear = (tid_start:tid_end) * detector.dt

    for dim = 1:3
        @views interp = LinearInterpolation(t_ret[data_start:data_end], E_ret[dim, data_start:data_end], extrapolation_bc=0)
        if synchronize
            @views E_linear = interp(t_linear)
            lock(detector.lock)
            @inbounds @views @. detector.E[dim, tid_start:tid_end] .+= E_linear
            unlock(detector.lock)
        else
            @inbounds @views @. detector.E[dim, tid_start:tid_end] .+= interp(t_linear)
        end
    end
end

function getspectrum(
    detector::Detector,
    ωₘₐₓ::Float64,
    plan::FFTW.FFTWPlan
)
    nfft = detector.nt

    Δω = 2pi / detector.dt / nfft
    ax_ω = Vector{Float64}(0:Δω:ωₘₐₓ+Δω)
    nω = length(ax_ω)

    # fft
    dI_dωdΩ = plan * detector.E
    # square sum 3 dims
    @views dI_dωdΩ = sum(real(dI_dωdΩ[:, 1:nω]).^2 .+ imag(dI_dωdΩ[:, 1:nω]).^2, dims=1)
    # normalizationn of fft
    dI_dωdΩ .*= detector.dt^2
    # physical factor, result is in unit of hbar
    dI_dωdΩ .*= α / (4 * pi^2)

    return Spectrum(dI_dωdΩ[:], ax_ω)
end

end
