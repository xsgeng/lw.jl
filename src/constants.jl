const α = 0.0072973525693
# arb. units
const c = 1.0
const mₑ = 1.0
const e = 1.0
# const ħ = 1.0

const mₑ² = mₑ^2
const c² = c^2
const mₑ²c² = mₑ² * c²

const q = -e
const m = mₑ

const cSI = 2.99792458E8
