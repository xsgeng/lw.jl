export IncoherentSpectrum, CoherentSpectrum

"""
`IncoherentSpectrum(trajectories, n_detectors, dt_detector, dur_detector, nω, ωₘ; nthread, nfftfactor)`

The input `part` and `trajectories` can be created via `h5readparticles` and `extract(part)`.

Compute the spectrum observed by the `n_detectors::Vector{Vector{Float64}}` that contains
observation vectors `n`. `dt_detector` and `dur_detector` define the time interval between data points
and the duration of the data since the pulse arrives at the detector.

The computed spectrums are interpolated to the `[0, ωₘ]` region with
`nω` points. The specrums of each direction are returned as a vector.

`nfftfactor` is multiplied to `nt_detector` as the number of fft points.

"""
function IncoherentSpectrum(
    trajectories::Vector{Trajectory},
    n_detectors::Vector{Vector{Float64}},
    dt_detector::Float64,
    dur_detector::Float64,
    nω::Int,
    ωₘ::Float64;
    nthread::Int = Threads.nthreads(),
    nfftfactor::Int=4,
)
@assert dt_detector <= π/ωₘ "dt_detector must be smaller than π/ωₘ"

npart = length(trajectories)
ndetector = length(n_detectors)

spec_out = [Spectrum(zeros(nω), LinRange(0, ωₘ, nω)) for _ = 1:ndetector]

# increase to 2ⁿ points, the duration is also increased
nt_detector = nextpow(2, dur_detector / dt_detector*nfftfactor)
t_detector = (0:nt_detector - 1)*dt_detector
println("nt_detector set to $nt_detector")

signal = Threads.Atomic{Int}(1)
# threads compute the fields
Threads.@threads for ith = 1:ndetector
    # real fft plan
    plan = plan_rfft(zeros(3, nt_detector), 2)
    # buffer
    detector = Detector(nt_detector, dt_detector, t_detector[end], t_detector)

    idetector = signal[]
    signal[] += 1

    println("processing #$idetector detector")
    n = n_detectors[idetector]

    for ipart = 1:length(trajectories)
        Eret, tret = getfields(trajectories[ipart], n)
        tret .-= tret[1]

        # set to zero before start
        reset!(detector)

        # fields at detector
        interpfield!(detector, Eret, tret)
        # spectrum
        spec = getspectrum(detector, ωₘ, plan)
        add!(spec_out[idetector], spec)
    end

    # averaging to per particle
    spec_out[idetector].amp ./= npart

end # end for
return spec_out


end

"""
`CoherentSpectrum(trajectories, part, detectors, dt_detector, dur_detector, nω, ωₘ)`

The input `part` and `trajectories` can be created via `h5readparticles` and `extract(part)`.

Compute the spectrum observed by the `detectors::Vector{Vector{Float64}}` that contains
observation vectors `n`. `dt_detector` and `dur_detector` define the time interval between data points
and the duration of the data since the pulse arrives at the detector.

The computed spectrums are interpolated to the `[0, ωₘ]` region with
`nω` points. The specrums of each direction are returned as a vector.

"""
function CoherentSpectrum(
    trajectories::Vector{Trajectory},
    n_detectors::Vector{Vector{Float64}},
    dt_detector::Float64,
    dur_detector::Float64,
    nω::Int,
    ωₘ::Float64;
    nthread::Int = Threads.nthreads(),
)
@assert dt_detector <= π/ωₘ "dt_detector must be smaller than π/ωₘ"

npart = length(trajectories)
ndetector = length(n_detectors)

spec_out = [Spectrum(zeros(nω), LinRange(0, ωₘ, nω)) for _ = 1:ndetector]

# increase to 2ⁿ points, the duration is also increased
nt_detector = nextpow(2, dur_detector / dt_detector)
t_detector = (0:nt_detector - 1)*dt_detector
println("nt_detector set to $nt_detector")

detector = Detector(nt_detector, dt_detector, t_detector[end], t_detector)

# set num of threads before plan
FFTW.set_num_threads(nthread)
plan = plan_rfft(detector.E, 2)

# threads compute the fields
for idetector = 1:ndetector
    println("processing #$idetector detector")

    n = n_detectors[idetector]
    # pulse duration
    rad_start, _, _ = radtime(n, trajectories)
    # set to zero before start
    reset!(detector)

    ipart = Threads.Atomic{Int}(0)
    @Threads.threads for ith = 1:nthread
        while ipart[] < npart
            ipart[] += 1
            Eret, tret = getfields(trajectories[ipart[]], n)
            tret .-= rad_start
            # fields at detector
            interpfield!(detector, Eret, tret, synchronize=true)
            
        end
    end
    spec = getspectrum(detector, ωₘ, plan)
    add!(spec_out[idetector], spec)

    # averaging to per particle
    spec_out[idetector].amp ./= npart

end # end for

return spec_out
end
