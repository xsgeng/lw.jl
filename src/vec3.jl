export cross, dot
import LinearAlgebra.cross
@inline function cross(a::Array{Float64,2}, b::Array{Float64,2})
    if size(a) != size(b)
        error("a != b")
    end
    if size(a, 1) != 3
        error("dim of a != 3xN")
    end

    c = zeros(size(a))
    @views @. c[1, :] .= a[2, :] .* b[3, :] - a[3, :] .* b[2, :]
    @views @. c[2, :] .= a[3, :] .* b[1, :] - a[1, :] .* b[3, :]
    @views @. c[3, :] .= a[1, :] .* b[2, :] - a[2, :] .* b[1, :]
    return c
end

@inline function cross(a::Vector{Float64}, b::Array{Float64,2})
    if size(a, 1) != 3
        error("dim of a != 3xN")
    end
    c = zeros(size(b))
    @views @. c[1, :] .= a[2] .* b[3, :] - a[3] .* b[2, :]
    @views @. c[2, :] .= a[3] .* b[1, :] - a[1] .* b[3, :]
    @views @. c[3, :] .= a[1] .* b[2, :] - a[2] .* b[1, :]

    return c

end

import LinearAlgebra.dot
@inline function dot(a::Array{Float64,2}, b::Array{Float64,2})
    if size(a, 1) != 3
        error("dim of a != 3xN")
    end
    return sum(a .* b, dims = 1)
end
@inline function dot(a::Vector{Float64}, b::Array{Float64,2})
    if size(a, 1) != 3
        error("dim of a != 3xN")
    end
    return sum(a .* b, dims = 1)
end
